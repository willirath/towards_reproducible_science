# Towards _Efficient & Reproducible_ Science

[![pipeline status](https://gitlab.com/willirath/towards_reproducible_science/badges/master/pipeline.svg)](https://gitlab.com/willirath/towards_reproducible_science/commits/master)
Latest version of the slides: <https://willirath.gitlab.io/towards_reproducible_science/>

> ## Abstract
>
> This presentation is about the every-day benefits of reproducible science:
> making the scientific workflow more efficient by facilitating communication
> and collaboration between individual scientists or among small groups.  This
> is often overlooked as scientists are more and more forced to enact
> reproducibility by a growing public debate on the “reproducibilty crisis”, by
> journals demanding data published along with the manuscript, or by funding
> agencies more vigorously enforcing open-data policies.
>
> After demonstrating how reproducibility is often undermined, the talk seeks
> to provide a simple framework for assessing the reproducibility of scientific
> workflows and to give an overview of existing building blocks for
> reproducible science at GEOMAR and beyond.
>
> This talks addresses scientists at any stage in their career and in any
> organisational role, and is also suited for students and technical support
> staff.
